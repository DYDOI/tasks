import React from 'react';

const DataComponent = (props) => {
  const folders = [
  {
    name: '1',
    filesContains: 5,
    folders: [
      {
        name: '1-1',
        filesContains: 2,
        folders: [
          {
            name: '1-1-1',
            filesContains: 1
          },
          {
            name: '1-1-2',
            filesContains: 1,
            folders: [null]
          },
        ]
      },
      {
        name: '1-2',
        filesContains: 3,
        folders: [
          {
            name: '1-2-1',
            filesContains: 3,
            folders: [null]
          }
        ]
      }
    ]
  },
  {
    name: '2',
    filesContains: 7,
    folders: [
      {
        name: '2-1',
        filesContains: 1,
        folders: [null]
      },
      {
        name: '2-2',
        filesContains: 2,
        folders: [null]
      },
      {
        name: '2-3',
        filesContains: 0,
        folders: [null]
      },
      {
        name: '2-4',
        filesContains: 4,
        folders: [
          {
            name: '2-4-1',
            filesContains: 3,
            folders: [
              {
                name: '2-4-1-1',
                filesContains: 0,
                folders: [
                  {
                    name: '2-4-1-1-1',
                    filesContains: 0
                  }
                ]
              }
            ]
          },
          {
            name: '2-4-2',
            filesContains: 1,
            folders: [null]
          }
        ]
      }
    ]
  },
  {
    name: '3',
    filesContains: 5,
    folders: [null]
  },
  {
    name: '4',
    filesContains: 1,
    folders: [
      {
        name: '4-1',
        filesContains: 0,
        folders: [null]
      },
      {
        name: '4-2',
        filesContains: 1,
        folders: [
          {
            name: '4-2-1',
            filesContains: 1,
            folders: [null]
          },
          {
            name: '4-2-2',
            filesContains: 0,
            folders: [
              {
                name: '4-2-2-1',
                filesContains: 0,
                folders: [null]
              },
              {
                name: '4-2-2-2',
                filesContains: 0,
                folders: [
                  {
                    name: '4-2-2-2-1',
                    filesContains: 0,
                    folders: [null]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        name: '4-3',
        filesContains: 0,
        folders: [null]
      },
      {
        name: '4-4',
        filesContains: 0,
        folders: [null]
      },
      {
        name: '4-5',
        filesContains: 1,
        folders: [
          {
            name: '4-5-1',
            filesContains: 1,
            folders: [null]
          },
          {
            name: '4-5-2',
            filesContains: 0
          }
        ]
      }
    ]
  }
]
  return <button onClick={()=>props.render(folders,1)}>Show Tree</button>
}

export default DataComponent;
