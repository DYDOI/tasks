import React, {Component} from 'react';
import DataComponent from './data';

class App extends Component {

  printSubLevel = (subtree, tabsAmount) => {
    console.log((" ".repeat(tabsAmount)) + "Folder " + subtree.name);
  }

  renderTree = (tree, tabsAmount) => {
    if (typeof tree !== "object") {
      console.log("не, брат, херня какая-то");
      return;
    }

    let j = 0,
    depth = 0;
    tree.forEach((subtree, i, currentArray) => {
      this.printSubLevel(subtree, tabsAmount)

      if (subtree.folders && subtree.folders[0]) {
        const walkThroughSubTree = (subtree,tabsAmount) => {
          subtree.forEach((subtree, i) => {
            this.printSubLevel(subtree, tabsAmount + 1)
            if (subtree.folders && subtree.folders[0]) {
              while (j < subtree.folders.length) {
                this.printSubLevel(subtree.folders[j], tabsAmount + 2);
                if (subtree.folders[j].folders && subtree.folders[j].folders[0]) {
                  while (depth < subtree.folders[j].folders.length) {
                    walkThroughSubTree(subtree.folders[j].folders,tabsAmount + 3);
                    depth += 1;
                  }
                }
                j += 1;
              }
              j = 0;
            }
          });
        }
        walkThroughSubTree(subtree.folders,tabsAmount);
      }
    });
  }


  render() {
    return <DataComponent render = {this.renderTree}/>
  }
}

export default App;
